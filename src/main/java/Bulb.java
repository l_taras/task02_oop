public class Bulb extends Electronics {
    public Bulb(String name, int power, EnergyEfficiency energyEfficiency) {
        super(name, power, energyEfficiency);
    }
}
