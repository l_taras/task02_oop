import java.util.StringJoiner;

public abstract class Electronics implements OnOff {
    private String name;
    private int power;
    private boolean isOn;
    private EnergyEfficiency energyEfficiency;

    public Electronics(String name, int power, EnergyEfficiency energyEfficiency) {
        this.name = name;
        this.power = power;
        this.isOn = false;
        this.energyEfficiency = energyEfficiency;
    }

    @Override
    public void On() {
        isOn = true;
    }

    @Override
    public void Off() {
        isOn = false;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public boolean isOn() {
        return isOn;
    }


    public EnergyEfficiency getEnergyEfficiency() {
        return energyEfficiency;
    }

    public void setEnergyEfficiency(EnergyEfficiency energyEfficiency) {
        this.energyEfficiency = energyEfficiency;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append(name);
        sb.append(", power=").append(power);
        sb.append(", isOn=").append(isOn);
        sb.append(", energyEfficiency=").append(energyEfficiency);
        sb.append('}');
        return sb.toString();
    }
}
