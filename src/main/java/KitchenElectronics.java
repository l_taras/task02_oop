import java.util.StringJoiner;

public class KitchenElectronics extends Electronics {
    private Material material;

    public KitchenElectronics(String name, int power, EnergyEfficiency energyEfficiency, Material material) {
        super(name, power, energyEfficiency);
        this.material = material;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }


}
