public class Light extends Electronics {
    Bulb[] bulbs;

    public Light(String name, Bulb... bulbs) {

        super(name, 0, EnergyEfficiency.D);
        this.bulbs = bulbs;
        int power = 0;
        for (Bulb bulb : bulbs) {
            power += bulb.getPower();
        }
        this.setPower(power);
    }

    public Bulb[] getBulbs() {
        return bulbs;
    }

    public void setBulbs(Bulb[] bulbs) {
        this.bulbs = bulbs;
    }
}
