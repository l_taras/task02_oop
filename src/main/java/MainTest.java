import java.util.ArrayList;

public class MainTest {
    public static void main(String[] args) {
        ArrayList<Electronics> list = new ArrayList<Electronics>();
        list.add(new Bulb("LED lamp", 5, EnergyEfficiency.A));
        list.add(new Bulb("LED lamp", 10, EnergyEfficiency.A));
        list.add(new Bulb("LED lamp", 7, EnergyEfficiency.A));
        list.add(new Bulb("LED lamp", 5, EnergyEfficiency.A));
        list.add(new Bulb("LED lamp", 12, EnergyEfficiency.A));
        list.add(new Bulb("lamp", 60, EnergyEfficiency.D));
        list.add(new Bulb("lamp", 40, EnergyEfficiency.D));
        list.add(new Bulb("lamp", 75, EnergyEfficiency.D));
        list.add(new KitchenElectronics("Oven", 3000, EnergyEfficiency.C, Material.STAINLESS));
        list.add(new KitchenElectronics("Microwave", 2000, EnergyEfficiency.C, Material.STAINLESS));
        list.add(new KitchenElectronics("Blander", 1000, EnergyEfficiency.B, Material.PLASTIC));
        list.add(new KitchenElectronics("Cooker", 800, EnergyEfficiency.B, Material.PLASTIC));
        list.add(new KitchenElectronics("Juicer", 700, EnergyEfficiency.B, Material.PLASTIC));
        list.add(new KitchenElectronics("Refrigerator", 120, EnergyEfficiency.A, Material.STAINLESS));
        list.add(new KitchenElectronics("Kettle", 1800, EnergyEfficiency.D, Material.GLASS));
        list.add(new Vacuum("Vacuum Cleaner", 1500, EnergyEfficiency.B, 75));
        list.add(new OfficeElectronics("Condotioner", 2500, EnergyEfficiency.D));
        list.add(new OfficeElectronics("Laptop", 140, EnergyEfficiency.A));
        list.add(new OfficeElectronics("Printer", 80, EnergyEfficiency.A));
        list.add(new OfficeElectronics("TV", 110, EnergyEfficiency.A));
        list.add(new Bulb("LED lamp", 5, EnergyEfficiency.A));
        list.add(new Bulb("LED lamp", 5, EnergyEfficiency.A));
        list.add(new Light("Light SSS", new Bulb("Bulb", 60, EnergyEfficiency.D), new Bulb("Bulb", 40, EnergyEfficiency.D)));


        Manager manager = new Manager(list);
        manager.randomOnOff();
        System.out.println("Total power= " + manager.totalPower());
        System.out.println("Total power is On electronics= " + manager.totalPower(true));
        System.out.println("Total power is Off electronics= " + manager.totalPower(false));

        for (Electronics electronics : manager.sortAllByPower()) {
            System.out.println(electronics);
        }
        System.out.println("\n" + "sortIsOnByPower:");
        for (Electronics electronics : manager.sortIsOnByPower()) {
            System.out.println(electronics);
        }
        System.out.println("\n" + "findByParameters");
        for (Electronics electronics : manager.findByParameters(50, 1200, true)) {
            System.out.println(electronics);
        }
    }


}
