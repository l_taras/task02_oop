import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public final class Manager {
    private ArrayList<Electronics> list = new ArrayList<Electronics>();

    public Manager(ArrayList<Electronics> list) {
        this.list = list;
    }

    /**
     * @return Sorted all electronics by power either off or on
     */
    public ArrayList<Electronics> sortAllByPower() {
        ArrayList<Electronics> list = new ArrayList<Electronics>();
        list.addAll(this.list);
        Collections.sort(list, (o1, o2) -> o1.getPower() - o2.getPower());
        Collections.reverse(list);
        return list;
    }

    /**
     * @return Sorted all electronics that is ON by power
     */
    public ArrayList<Electronics> sortIsOnByPower() {
        ArrayList<Electronics> list = new ArrayList<Electronics>();
        for (Electronics electronics : this.list) {
            if (electronics.isOn()) {
                list.add(electronics);
            }
        }
        Collections.sort(list, (o1, o2) -> o1.getPower() - o2.getPower());
        Collections.reverse(list);
        return list;
    }


    /**
     * Random On/Off electronics
     */
    public void randomOnOff() {
        Random random = new Random();
        for (Electronics electronics : list) {
            if (random.nextBoolean()) {
                electronics.On();
            } else {
                electronics.Off();
            }
        }
    }

    /**
     * @return Total power
     */
    public int totalPower() {
        int totalPower = 0;
        for (Electronics electronics : list) {
            totalPower += electronics.getPower();
        }
        return totalPower;
    }

    /**
     * @param isOn either is On electronics
     * @return Total power is Onn/Off electronics
     */
    public int totalPower(boolean isOn) {
        int totalPower = 0;
        for (Electronics electronics : list) {
            if (electronics.isOn() == isOn) {
                totalPower += electronics.getPower();
            }
        }
        return totalPower;
    }

    /**
     * @param lowPower  from Power
     * @param highPower to Power
     * @param isOn      eiher is On
     * @return Found by parameters
     */
    public ArrayList<Electronics> findByParameters(int lowPower, int highPower, boolean isOn) {
        ArrayList<Electronics> list = new ArrayList<Electronics>();
        for (Electronics electronics : this.list) {
            if (electronics.getPower() >= lowPower && electronics.getPower() <= highPower && electronics.isOn() == isOn) {
                list.add(electronics);
            }
        }
        Collections.sort(list, (o1, o2) -> o1.getPower() - o2.getPower());
        Collections.reverse(list);
        return list;
    }

    public ArrayList<Electronics> findByParameters(int lowPower, int highPower) {
        ArrayList<Electronics> list = new ArrayList<Electronics>();
        for (Electronics electronics : this.list) {
            if (electronics.getPower() >= lowPower && electronics.getPower() <= highPower) {
                list.add(electronics);
            }
        }
        Collections.sort(list, (o1, o2) -> o1.getPower() - o2.getPower());
        Collections.reverse(list);
        return list;
    }

    public ArrayList<Electronics> getList() {
        return list;
    }

}
