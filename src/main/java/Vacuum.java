public class Vacuum extends Electronics {
    private int noise;


    public Vacuum(String name, int power, EnergyEfficiency energyEfficiency, int noise) {
        super(name, power, energyEfficiency);
        this.noise = noise;
    }

    public int getNoise() {
        return noise;
    }

    public void setNoise(int noise) {
        this.noise = noise;
    }
}
